!
version 12.4
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet0/1
 no ip address
 ip access-group ADMIN_ONLY_ACCESS_TO_SERVER in
 duplex auto
 speed auto
!
interface FastEthernet0/1.2
 encapsulation dot1Q 2
 ip address 172.16.0.1 255.255.255.0
 ip access-group ADMIN_ONLY_ACCESS_TO_SERVER in
!
interface FastEthernet0/1.3
 encapsulation dot1Q 3
 ip address 172.16.1.254 255.255.255.0
 ip access-group ADMIN_ONLY_ACCESS_TO_SERVER in
!
interface FastEthernet0/1.10
 encapsulation dot1Q 10
 ip address 172.16.2.254 255.255.255.0
 ip access-group ADMIN_ONLY_ACCESS_TO_SERVER in
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 no ip address
!
ip classless
!
ip flow-export version 9
!
!
ip access-list extended ADMIN_ONLY_ACCESS_TO_SERVER
 permit ip 172.16.0.0 0.0.0.255 172.16.1.0 0.0.0.255
 permit icmp 172.16.0.0 0.0.0.255 172.16.1.0 0.0.0.255
 permit tcp any 172.16.1.0 0.0.0.255 eq 80
 permit tcp any 172.16.1.0 0.0.0.255 eq 443
 permit tcp any 172.16.1.0 0.0.0.255 eq 53
 permit udp any 172.16.1.0 0.0.0.255 eq 53
 permit tcp any 172.16.1.0 0.0.0.255 eq 21
 permit udp any 172.16.1.0 0.0.0.255 eq 21
 deny ip any 172.16.1.0 0.0.0.255
 deny icmp any 172.16.1.0 0.0.0.255
 permit icmp any any
 permit ip any any
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

